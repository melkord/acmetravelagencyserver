<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


$router->group(['prefix' => 'api/airports'], function () use ($router) {
    $router->get('/', ['uses' => 'AirportController@showAllAirports']);

    $router->get('/search/{query}', ['uses' => 'AirportController@searchAirports']);

    $router->get('/{id}', ['uses' => 'AirportController@showOneAirport']);

    $router->post('', ['uses' => 'AirportController@create']);

    $router->delete('/{id}', ['uses' => 'AirportController@delete']);

    $router->put('/{id}', ['uses' => 'AirportController@update']);

});


$router->group(['prefix' => 'api/flights'], function () use ($router) {
    $router->get('/{codeFrom}-{codeTo}', ['uses' => 'FlightController@findFlights']);

    $router->get('', ['uses' => 'FlightController@showAllFlights']);

});
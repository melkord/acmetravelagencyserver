# Acme Travel Agency Server

## Table of content
- [Acme Travel Agency Server](#acme-travel-agency-server)
  * [Table of content](#table-of-content)
  * [Local installation](#local-installation)
  * [API](#api)
    + [Flights](#flights)
      - [GET /api/flights](#get-apiflights)
      - [GET /api/flights/{codeFrom}-{codeTo}](#get-apiflightscodefrom-codeto)
    + [Airport](#airport)
      - [GET /api/airports/search/{query}](#get-apiairportssearchquery)
      - [GET /api/airports](#get-apiairports)
      - [GET /api/airports/{id}](#get-apiairportsid)
      - [POST /api/airports](#post-apiairports)
      - [DELETE /api/airports](#delete-apiairports)
      - [PUT /api/airports/{id}](#put-apiairportsid)
## Local installation
To install properly this project you need to have `composer` and `php >= 8.1` installed.

1. Create file `.env` using `.env.example` as template

2. Launch `composer install`

3. Launch db migration and seeding `php artisan migrate --seed` _(you can use `php artisan migrate --seed` if you want to re-run the migration and seeding)_

4.  Launch server `php -S localhost:8000 -t public`

5. Enjoy the APIs :)

## API
### Flights
#### GET /api/flights
Get all flights stored

**Response example:**
```
[
    {
        "code_departure": "FCO",
        "code_arrival": "MXP",
        "price": "100.00"
    },
    {
        "code_departure": "FCO",
        "code_arrival": "LHR",
        "price": "150.50"
    }
]
```
#### GET /api/flights/{codeFrom}-{codeTo}
Search all flights from one airport to another one, with at most one stopover ordered by price from lower to higher

**Response example:**
```
[
    {
        "totalPrice": "90.25",
        "stopover": null
    },
    {
        "totalPrice": "261.25",
        "stopover": "FCO"
    },
    {
        "totalPrice": "360.75",
        "stopover": "MXP"
    },
    {
        "totalPrice": "580",
        "stopover": "ZRH"
    }
]
```
### Airport

#### GET /api/airports/search/{query}
Search an airport using the name as matcher property

**Response example:**
```
[
    {
        "id": 2,
        "name": "Milano Malpensa",
        "code": "MXP",
        "lat": "45.6306",
        "lng": "8.7281"
    },
    {
        "id": 3,
        "name": "Londra Heathrow",
        "code": "LHR",
        "lat": "51.4700",
        "lng": "-0.4543"
    },
    {
        "id": 4,
        "name": "Parigi Charles de Gaulle",
        "code": "CDG",
        "lat": "49.0097",
        "lng": "2.5479"
    },
    {
        "id": 5,
        "name": "Francoforte sul Meno",
        "code": "FRA",
        "lat": "50.0333",
        "lng": "8.5706"
    },
    {
        "id": 6,
        "name": "Amsterdam Schiphol",
        "code": "AMS",
        "lat": "52.3105",
        "lng": "4.7683"
    },
```
#### GET /api/airports
Get all airports

**Response example:**
```
[
    {
        "id": 1,
        "name": "Roma Fiumicino",
        "code": "FCO",
        "lat": "41.8003",
        "lng": "12.2389"
    },
    {
        "id": 2,
        "name": "Milano Malpensa",
        "code": "MXP",
        "lat": "45.6306",
        "lng": "8.7281"
    }
]
```
#### GET /api/airports/{id}
Get a specific airport

**Response example:**
```
{
    "id": 1,
    "name": "Roma Fiumicino",
    "code": "FCO",
    "lat": "41.8003",
    "lng": "12.2389"
}
```
#### POST /api/airports
Create a new airport

**Payload example:**
```
{
    "name": "Foo Airport",
    "code": "FOO",
    "lat": "123",
    "lng": "456"
}
```
**Response example:**
```
{
    "name": "Foo Airport",
    "code": "FOO",
    "lat": "123",
    "lng": "456",
    "id": 62
}
```
#### DELETE /api/airports
Delete a specific airport

**Response example:**
```
Deleted Successfully
```
#### PUT /api/airports/{id}
Edit a specific airport
**Payload example:**
```
{
    "name": "Bar Airport",
    "code": "FOO",
    "lat": "123",
    "lng": "456",
    "id": 62
}
```
You can use in the payload a sub-set of the airport property

**Response example:**
```
{
    "name": "Bar Airport",
    "code": "FOO",
    "lat": "123",
    "lng": "456",
    "id": 62
}
```




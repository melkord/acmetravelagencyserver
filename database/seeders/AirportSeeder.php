<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AirportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $airports = [
            [
                'name' => 'Roma Fiumicino',
                'code' => 'FCO',
                'lat' => '41.8003',
                'lng' => '12.2389',
            ],
            [
                'name' => 'Milano Malpensa',
                'code' => 'MXP',
                'lat' => '45.6306',
                'lng' => '8.7281',
            ],
            [
                'name' => 'Londra Heathrow',
                'code' => 'LHR',
                'lat' => '51.4700',
                'lng' => '-0.4543',
            ],
            [
                'name' => 'Parigi Charles de Gaulle',
                'code' => 'CDG',
                'lat' => '49.0097',
                'lng' => '2.5479',
            ],
            [
                'name' => 'Francoforte sul Meno',
                'code' => 'FRA',
                'lat' => '50.0333',
                'lng' => '8.5706',
            ],
            [
                'name' => 'Amsterdam Schiphol',
                'code' => 'AMS',
                'lat' => '52.3105',
                'lng' => '4.7683',
            ],
            [
                'name' => 'Madrid Barajas',
                'code' => 'MAD',
                'lat' => '40.4719',
                'lng' => '-3.5626',
            ],
            [
                'name' => 'Atene Eleftherios Venizelos',
                'code' => 'ATH',
                'lat' => '37.9364',
                'lng' => '23.9472',
            ],
            [
                'name' => 'Bangkok Suvarnabhumi',
                'code' => 'BKK',
                'lat' => '13.6929',
                'lng' => '100.7508',
            ],
            [
                'name' => 'Beijing Capital',
                'code' => 'PEK',
                'lat' => '40.0799',
                'lng' => '116.6031',
            ],
            [
                'name' => 'New York John F. Kennedy',
                'code' => 'JFK',
                'lat' => '40.6413',
                'lng' => '-73.7781',
            ],
            [
                'name' => 'Tokyo Haneda',
                'code' => 'HND',
                'lat' => '35.5494',
                'lng' => '139.7798',
            ],
            [
                'name' => 'Sydney Kingsford-Smith',
                'code' => 'SYD',
                'lat' => '-33.9461',
                'lng' => '151.1772',
            ],
            [
                'name' => 'Città del Messico Benito Juárez',
                'code' => 'MEX',
                'lat' => '19.4363',
                'lng' => '-99.0720',
            ],
            [
                'name' => 'Dubai Internazionale',
                'code' => 'DXB',
                'lat' => '25.2532',
                'lng' => '55.3657',
            ],
            [
                'name' => 'Mosca Sheremetyevo',
                'code' => 'SVO',
                'lat' => '55.9726',
                'lng' => '37.4142',
            ],
            [
                'name' => 'Istanbul Atatürk',
                'code' => 'IST',
                'lat' => '40.9767',
                'lng' => '28.8150',
            ],
            [
                'name' => 'Toronto Pearson',
                'code' => 'YYZ',
                'lat' => '43.6777',
                'lng' => '-79.6248',
            ],
            [
                'name' => 'Melbourne Tullamarine',
                'code' => 'MEL',
                'lat' => '-37.6690',
                'lng' => '144.8410',
            ],
            [
                'name' => 'Zurigo Kloten',
                'code' => 'ZRH',
                'lat' => '47.4647',
                'lng' => '8.5492',
            ],
        ];
        DB::table('airports')->insert(
            $airports
        );
    }
}
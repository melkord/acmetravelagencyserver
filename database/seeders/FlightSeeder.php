<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FlightSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('flights')->insert([
            ['code_departure' => 'FCO', 'code_arrival' => 'MXP', 'price' => '100.00'],
            ['code_departure' => 'FCO', 'code_arrival' => 'LHR', 'price' => '150.50'],
            ['code_departure' => 'FCO', 'code_arrival' => 'CDG', 'price' => '120.75'],
            ['code_departure' => 'MXP', 'code_arrival' => 'FCO', 'price' => '90.00'],
            ['code_departure' => 'MXP', 'code_arrival' => 'LHR', 'price' => '200.25'],
            ['code_departure' => 'MXP', 'code_arrival' => 'CDG', 'price' => '170.50'],
            ['code_departure' => 'LHR', 'code_arrival' => 'FCO', 'price' => '140.50'],
            ['code_departure' => 'LHR', 'code_arrival' => 'MXP', 'price' => '180.25'],
            ['code_departure' => 'LHR', 'code_arrival' => 'CDG', 'price' => '100.75'],
            ['code_departure' => 'CDG', 'code_arrival' => 'FCO', 'price' => '110.75'],
            ['code_departure' => 'CDG', 'code_arrival' => 'MXP', 'price' => '160.50'],
            ['code_departure' => 'CDG', 'code_arrival' => 'LHR', 'price' => '90.25'],
            ['code_departure' => 'SYD', 'code_arrival' => 'FCO', 'price' => '500.00'],
            ['code_departure' => 'SYD', 'code_arrival' => 'LHR', 'price' => '650.50'],
            ['code_departure' => 'SYD', 'code_arrival' => 'CDG', 'price' => '600.75'],
            ['code_departure' => 'SYD', 'code_arrival' => 'MXP', 'price' => '550.00'],
            ['code_departure' => 'MEL', 'code_arrival' => 'FCO', 'price' => '400.00'],
            ['code_departure' => 'MEL', 'code_arrival' => 'LHR', 'price' => '550.50'],
            ['code_departure' => 'MEL', 'code_arrival' => 'CDG', 'price' => '500.75'],
            ['code_departure' => 'MEL', 'code_arrival' => 'MXP', 'price' => '450.00'],
            ['code_departure' => 'SYD', 'code_arrival' => 'MEL', 'price' => '200.00'],
            ['code_departure' => 'SYD', 'code_arrival' => 'AMS', 'price' => '350.50'],
            ['code_departure' => 'SYD', 'code_arrival' => 'FRA', 'price' => '320.75'],
            ['code_departure' => 'SYD', 'code_arrival' => 'IST', 'price' => '450.00'],
            ['code_departure' => 'MEL', 'code_arrival' => 'SYD', 'price' => '180.00'],
            ['code_departure' => 'MEL', 'code_arrival' => 'LAX', 'price' => '550.00'],
            ['code_departure' => 'MEL', 'code_arrival' => 'JFK', 'price' => '600.50'],
            ['code_departure' => 'MEL', 'code_arrival' => 'DXB', 'price' => '520.75'],
            ['code_departure' => 'FCO', 'code_arrival' => 'AMS', 'price' => '180.50'],
            ['code_departure' => 'FCO', 'code_arrival' => 'FRA', 'price' => '220.75'],
            ['code_departure' => 'FCO', 'code_arrival' => 'IST', 'price' => '200.00'],
            ['code_departure' => 'FCO', 'code_arrival' => 'JFK', 'price' => '350.50'],
            ['code_departure' => 'AMS', 'code_arrival' => 'FCO', 'price' => '160.00'],
            ['code_departure' => 'AMS', 'code_arrival' => 'MXP', 'price' => '210.00'],
            ['code_departure' => 'AMS', 'code_arrival' => 'CDG', 'price' => '180.50'],
            ['code_departure' => 'AMS', 'code_arrival' => 'LHR', 'price' => '200.75'],
            ['code_departure' => 'MEL', 'code_arrival' => 'AMS', 'price' => '650.00'],
            ['code_departure' => 'AMS', 'code_arrival' => 'SYD', 'price' => '700.00'],
            ['code_departure' => 'SYD', 'code_arrival' => 'ZRH', 'price' => '480.00'],
            ['code_departure' => 'ZRH', 'code_arrival' => 'SYD', 'price' => '520.00'],
            ['code_departure' => 'CDG', 'code_arrival' => 'ZRH', 'price' => '320.00'],
            ['code_departure' => 'ZRH', 'code_arrival' => 'CDG', 'price' => '300.00'],
            ['code_departure' => 'LHR', 'code_arrival' => 'ZRH', 'price' => '280.00'],
            ['code_departure' => 'ZRH', 'code_arrival' => 'LHR', 'price' => '260.00'],
            ['code_departure' => 'AMS', 'code_arrival' => 'ZRH', 'price' => '200.00'],
            ['code_departure' => 'ZRH', 'code_arrival' => 'AMS', 'price' => '180.00'],
            ['code_departure' => 'FCO', 'code_arrival' => 'ZRH', 'price' => '150.00'],
            ['code_departure' => 'ZRH', 'code_arrival' => 'FCO', 'price' => '130.00']
        ]);
    }
}
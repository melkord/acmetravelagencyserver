<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flight extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code_departure',
        'code_arrival',
        'price',
    ];
    public $timestamps = false;


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

}
<?php

namespace App\Http\Controllers;

use App\Airport;
use Illuminate\Http\Request;

class AirportController extends Controller
{

    public function showAllAirports()
    {
        return response()->json(Airport::all());
    }
    public function searchAirports($query)
    {
        $response = Airport::where('name', 'LIKE', '%' . $query . '%')->get();

        return response()->json($response);
    }

    public function showOneAirport($id)
    {
        return response()->json(Airport::find($id));
    }

    public function create(Request $request)
    {
        $Airport = Airport::create($request->all());

        return response()->json($Airport, 201);
    }

    public function update($id, Request $request)
    {
        $Airport = Airport::findOrFail($id);
        $Airport->update($request->all());

        return response()->json($Airport, 200);
    }

    public function delete($id)
    {
        Airport::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}
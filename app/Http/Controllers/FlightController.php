<?php

namespace App\Http\Controllers;

use App\Flight;
use DB;
use Illuminate\Http\Request;

class FlightController extends Controller
{

    public function showAllFlights()
    {
        return response()->json(Flight::all());
    }
    public function findFlights($codeFrom, $codeTo)
    {
        $result = DB::table(function ($query) use ($codeFrom, $codeTo) {
            $query->select(DB::raw('f1.price + f2.price AS price'))
                ->addSelect(DB::raw("f2.code_departure AS stopover"))
                ->from('flights as f1')
                ->join('flights as f2', 'f1.code_arrival', '=', 'f2.code_departure')
                ->where('f1.code_departure', '=', $codeFrom)
                ->where('f2.code_arrival', '=', $codeTo);
        })
            ->union(DB::table(function ($query) use ($codeFrom, $codeTo) {
                $query->select('price AS price')
                    ->addSelect(DB::raw("NULL AS stopover"))
                    ->from('flights')
                    ->where('code_departure', '=', $codeFrom)
                    ->where('code_arrival', '=', $codeTo);
            }))
            ->orderByRaw('CAST(price AS DECIMAL) ASC')
            ->get();

        return response()->json($result);
    }
}